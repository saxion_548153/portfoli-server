import Database from "better-sqlite3";
import * as queries from "./db_queries.js";
import {createVar, createVarTable} from "./db_queries.js";

export let db;

try{
    db = new Database("src/Database/data.sqlite")
}catch (e) {
    console.log('Error initializing database', e);
    throw e;
}

db.prepare(queries.create_portfolio_table).run()
db.prepare(queries.create_skill_table).run()
db.prepare(queries.create_project_table).run()
db.prepare(queries.create_portfolio_skill_junction).run()
db.prepare(queries.create_portfolio_project_junction).run()
db.prepare(queries.createVarTable).run()
db.prepare(queries.createVar).run()



