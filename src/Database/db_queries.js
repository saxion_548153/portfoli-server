export const create_portfolio_table = `
        CREATE TABLE IF NOT EXISTS portfolio (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            description TEXT NOT NULL
            );`
export const create_skill_table = `
        CREATE TABLE IF NOT EXISTS skill (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            description TEXT NOT NULL
            );`

export const create_project_table = `
        CREATE TABLE IF NOT EXISTS project (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            description TEXT NOT NULL,
            organization TEXT NOT NULL
            );`
export const create_portfolio_skill_junction = `
        CREATE TABLE IF NOT EXISTS portfolio_skill (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            portfolio_id INTEGER,
            skill_id INTEGER,
            FOREIGN KEY (portfolio_id) REFERENCES portfolio (id) ON DELETE CASCADE,
            FOREIGN KEY (skill_id) REFERENCES skill (id) ON DELETE CASCADE

            );`
export const create_portfolio_project_junction = `
        CREATE TABLE IF NOT EXISTS portfolio_project (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            portfolio_id INTEGER,
            project_id INTEGER,
            FOREIGN KEY (portfolio_id) REFERENCES portfolio (id) ON DELETE CASCADE,
            FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE
            );`

export const createVarTable = `
        CREATE TABLE IF NOT EXISTS vars (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            portfolio_to_edit INTEGER
            )
`

export const createVar = `
        INSERT INTO vars (portfolio_to_edit) VALUES (0)
`


export const get_portfolio_to_edit = `SELECT portfolio_to_edit FROM vars WHERE id = 1`
export const set_portfolio_to_edit = `UPDATE vars SET portfolio_to_edit = ? WHERE id = 1`

export const insert_skill_to_junction = `INSERT INTO portfolio_skill(portfolio_id,skill_id) VALUES (?,(SELECT id FROM skill WHERE skill.name = ?));`

export const get_portfolio_id_by_id = `SELECT id FROM portfolio WHERE id = ? `;
export const get_portfolio_id_by_name = `SELECT id FROM portfolio WHERE name = ? `;

export const get_all_portfolios = `SELECT * FROM portfolio`
export const insert_portfolio = `INSERT INTO portfolio(name,description) VALUES (?,?);`


export const get_skill_id_by_name = `SELECT id FROM skill WHERE name = ? `;
export const get_skill_name_by_id = `SELECT name FROM skill WHERE id = ? `;

export const insert_skill = `INSERT INTO skill(name,description) VALUES (?,?);`

export const get_all_skills = `SELECT * FROM skill`

export const delete_skill = 'DELETE FROM skill WHERE id = ?'

export const get_skill = 'SELECT * FROM skill WHERE id = ?';

export const update_skill = 'UPDATE skill SET name = ?, description = ? WHERE id = ?'


// Project
export const get_project_id_by_name = `SELECT id FROM project WHERE name = ? `;
export const get_project_name_by_id = `SELECT name FROM project WHERE id = ? `;

export const insert_project = `INSERT INTO project(name,description,organization) VALUES (?,?,?);`

export const get_all_projects = `SELECT * FROM project`

export const delete_project = 'DELETE FROM project WHERE id = ?'

export const get_project = 'SELECT * FROM project WHERE id = ?';

export const update_project = 'UPDATE project SET name = ?, description = ?, organization = ? WHERE id = ?'


// Portfolio

export const get_portfolio_skill_by_id = `SELECT portfolio_id FROM portfolio_skill WHERE portfolio_id = ? AND skill_id = ?`;
export const get_portfolio_project_by_id = `SELECT portfolio_id FROM portfolio_project WHERE portfolio_id = ? AND project_id = ?`;

export const add_skill_to_portfolio = `INSERT INTO portfolio_skill(portfolio_id,skill_id) VALUES(?,?)`
export const add_project_to_portfolio = `INSERT INTO portfolio_project(portfolio_id,project_id) VALUES(?,?)`

export const get_portfolio_skills = `SELECT skill_id FROM portfolio_skill WHERE portfolio_id = ?`;
export const get_project_skills = `SELECT project_id FROM portfolio_skill WHERE portfolio_id = ?`;
