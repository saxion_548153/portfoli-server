import Database from 'better-sqlite3'
import {db} from './database.js'
import * as database_queries from './db_queries.js'
import {get_portfolio_project_by_id} from "./db_queries.js";


// Portfolio

export function get_all_portfolios(req, res) {
    let portfolios_query_result = db.prepare(database_queries.get_all_portfolios).all()

    res.status(200)
    res.json(portfolios_query_result)
}
export function insert_portfolio(req,res){

    let form_body = req.body;
    let portfolio_id_query_result = db.prepare(database_queries.get_portfolio_id_by_name).get(form_body.portfolio_name);

    if (portfolio_id_query_result === undefined || Object.keys(portfolio_id_query_result).length === 0) {

        db.prepare(database_queries.insert_portfolio).run(form_body.portfolio_name,form_body.portfolio_description)

        res.status(200)
        res.send("The portfolio has been added successfully")
    }
    else{
        res.status(406)
        res.send("Portfolio already exists")
    }
}

export function set_portfolio_to_edit(req, res) {
    db.prepare(database_queries.set_portfolio_to_edit).run(req.query.id)
    res.status(200)
    res.send("The portfolio to edit has been set")
}

export function get_portfolio_to_edit(req, res) {
    let id = db.prepare(database_queries.get_portfolio_to_edit).get()
    res.status(200)
    res.send(id)
}

export function add_skill_to_portfolio(req,res){

    let get_portfolio_skill_query_result = db.prepare(database_queries.get_portfolio_skill_by_id).get(req.query.port_id,req.query.skill_id)

    if (get_portfolio_skill_query_result === undefined || Object.keys(get_portfolio_skill_query_result).length === 0) {
        db.prepare(database_queries.add_skill_to_portfolio).run(req.query.port_id,req.query.skill_id)
        res.status(200)
        res.send("The skill has been added successfully")
    }
}

export function add_project_to_portfolio(req,res){

    console.log(req.query)
    let get_portfolio_skill_query_result = db.prepare(database_queries.get_portfolio_project_by_id).get(req.query.port_id,req.query.skill_id)

    if (get_portfolio_skill_query_result === undefined || Object.keys(get_portfolio_skill_query_result).length === 0) {
        db.prepare(database_queries.add_project_to_portfolio).run(req.query.port_id,req.query.skill_id)
        res.status(200)
        res.send("The skill has been added successfully")
    }
}

export function get_portfolio_skills(req,res){
    let get_portfolio_skills_query_result = db.prepare(database_queries.get_portfolio_skills).all(req.query.port_id)
    res.status(200)
    res.send(get_portfolio_skills_query_result)
}

export function get_portfolio_projects(req,res){
    let get_portfolio_skills_query_result = db.prepare(database_queries.get_portfolio_skills).all(req.query.port_id)
    res.status(200)
    res.send(get_portfolio_skills_query_result)
}


// Skill

export function create_new_skill(req,res){

    let requested_skill = req.body["skill"];

    let skill_id_query_result = db.prepare(database_queries.get_skill_id_by_name).get(requested_skill.name);

    if (skill_id_query_result === undefined || Object.keys(skill_id_query_result).length === 0) {

        db.prepare(database_queries.insert_skill).run(requested_skill.name,requested_skill.description)

        res.status(200)
        res.send("The skill was inserted successfully")
    }
    else{
        res.status(406)
        res.send("Skill already exists")
    }
}

// !!!!!!!!!!!!!!!!!!! ADD CHECK IF SKILL IS IN PORTFOLIO

export function delete_skill(req,res){
    let skill_name_query_result = db.prepare(database_queries.get_skill_name_by_id).get(req.body.id);

    console.log(skill_name_query_result)
    if (skill_name_query_result === undefined || Object.keys(skill_name_query_result).length === 0) {
        res.status(406)
        res.send("Skill could not be deleted it either no longer exists or it's part of a portfolio")
    }
    else{

        db.prepare(database_queries.delete_skill).run(req.body.id)
        res.status(200)
        res.send("The skill was deleted successfully")

    }

}

export function get_all_skills(req, res) {
    let skills_query_result = db.prepare(database_queries.get_all_skills).all()

    res.status(200)
    res.json(skills_query_result)
}

export function get_skill(req,res){
    let skill_query_result = db.prepare(database_queries.get_skill).all(req.query.id)
    res.status(200)
    res.json(skill_query_result)
}

export function update_skill(req,res){

    let skill_name_query_result = db.prepare(database_queries.get_skill_name_by_id).get(req.query.id);

    if (skill_name_query_result === undefined || Object.keys(skill_name_query_result).length === 0) {
        res.status(406)
        res.send("Skill could not be updated it no longer exists")
    }
    else{

        db.prepare(database_queries.update_skill).run(req.query.name,req.query.description,req.query.id)
        res.status(200)
        res.send("The skill was updated successfully")

    }

}

// Project

export function create_new_project(req,res){

    let requested_project = req.body["project"];
    console.log(req.body)

    let project_id_query_result = db.prepare(database_queries.get_project_id_by_name).get(requested_project.name);

    if (project_id_query_result === undefined || Object.keys(project_id_query_result).length === 0) {

        db.prepare(database_queries.insert_project).run(requested_project.name,requested_project.description,requested_project.organization)

        res.status(200)
        res.send("The skill was inserted successfully")
    }
    else{
        res.status(406)
        res.send("Skill already exists")
    }
}

// !!!!!!!!!!!!!!!!!!! ADD CHECK IF SKILL IS IN PORTFOLIO

export function delete_project(req,res){
    let project_name_query_result = db.prepare(database_queries.get_project_name_by_id).get(req.body.id);

    console.log(project_name_query_result)
    if (project_name_query_result === undefined || Object.keys(project_name_query_result).length === 0) {
        res.status(406)
        res.send("Skill could not be deleted it either no longer exists or it's part of a portfolio")
    }
    else{

        db.prepare(database_queries.delete_project).run(req.body.id)
        res.status(200)
        res.send("The skill was deleted successfully")

    }

}

export function get_all_projects(req, res) {
    let project_query_result = db.prepare(database_queries.get_all_projects).all()

    res.status(200)
    res.json(project_query_result)
}

export function get_project(req,res){
    let project_query_result = db.prepare(database_queries.get_project).all(req.query.id)
    res.status(200)
    res.json(project_query_result)
}

export function update_project(req,res){

    let project_name_query_result = db.prepare(database_queries.get_project_name_by_id).get(req.query.id);

    if (project_name_query_result === undefined || Object.keys(project_name_query_result).length === 0) {
        res.status(406)
        res.send("Skill could not be updated it no longer exists")
    }
    else{
        console.log(req.query)
        db.prepare(database_queries.update_project).run(req.query.name,req.query.description,req.query.organization,req.query.id)
        res.status(200)
        res.send("The skill was updated successfully")

    }

}