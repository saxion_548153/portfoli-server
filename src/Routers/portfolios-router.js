import express from "express";
import * as portfolios_controller from "../Controllers/portfolios-controller.js"
import {get_portfolio_projects, get_portfolio_skills} from "../Controllers/portfolios-controller.js";

const router = express.Router();

router.get("/get/all",(req,res)=>{
    portfolios_controller.get_all_portfolios(req,res);
})

router.post("/add",(req,res)=>{
    portfolios_controller.insert_portfolio(req,res)
})

router.put("/update/to-edit",(req,res)=>{
    portfolios_controller.set_portfolio_to_edit(req,res)
})
router.get("/get/to-edit",(req,res)=>{
    portfolios_controller.get_portfolio_to_edit(req,res)
})

router.post("/add/skill",(req,res)=>{
    portfolios_controller.add_skill_to_portfolio(req,res)
})

router.post("/add/project",(req,res)=>{
    portfolios_controller.add_project_to_portfolio(req,res)
})

router.get("/get/skills",(req,res)=>{
    portfolios_controller.get_portfolio_skills(req,res)
})

router.get("/get/projects",(req,res)=>{
    portfolios_controller.get_portfolio_projects(req,res)
})

export default router;