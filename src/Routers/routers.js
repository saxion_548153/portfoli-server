import skill_router from "./skills-router.js"
import portfolio_router from "./portfolios-router.js"
import project_router from "./project-router.js"

export let skills_router = skill_router
export let portfolios_router = portfolio_router
export let projects_router = project_router