import express from 'express';
import * as skills_controller from '../Controllers/skills-controller.js';

const router = express.Router();

router.get("/get/all",(req, res) =>{
    skills_controller.get_all_skills(req, res);
})

router.get("/get",(req, res) =>{
    skills_controller.get_skill(req,res)
})

router.post("/add", (req, res) => {
    skills_controller.create_new_skill(req,res)
})

router.delete("/delete",(req,res)=>{
    skills_controller.delete_skill(req,res)
})

router.put("/update",(req,res)=>{
    skills_controller.update_skill(req,res)
})

export default router;