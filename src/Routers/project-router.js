import express from 'express';
import * as projects_controller from '../Controllers/project-controler.js';

const router = express.Router();

router.get("/get/all",(req, res) =>{
    projects_controller.get_all_projects(req, res);
})

router.get("/get",(req, res) =>{
    projects_controller.get_project(req,res)
})

router.post("/add", (req, res) => {
    projects_controller.create_new_project(req,res)
})

router.delete("/delete",(req,res)=>{
    projects_controller.delete_project(req,res)
})

router.put("/update",(req,res)=>{
    projects_controller.update_project(req,res)
})

export default router;