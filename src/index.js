export const port = 3000;

import express from 'express'
import cors from 'cors'

import * as routers from "./Routers/routers.js"
const app = express();

app.use(express.json({
    type: ['application/json', 'text/plain']
}));
app.use(express.urlencoded({ extended: true }))
app.use(cors())

app.use('/skill', routers.skills_router);
app.use('/portfolio', routers.portfolios_router)
app.use('/project', routers.projects_router)

app.listen(port, function (){
    console.log("Server is running on port " + port);
})