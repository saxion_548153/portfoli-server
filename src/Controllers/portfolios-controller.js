import * as databaseController from '../Database/database_helper.js'

export function get_all_portfolios(req, res) {
    databaseController.get_all_portfolios(req, res)
}

export function insert_portfolio(req, res) {
    databaseController.insert_portfolio(req, res)
}

export function set_portfolio_to_edit(req, res) {
    databaseController.set_portfolio_to_edit(req,res)
}

export function get_portfolio_to_edit(req, res) {
    databaseController.get_portfolio_to_edit(req,res)
}

export function add_skill_to_portfolio(req,res) {
    databaseController.add_skill_to_portfolio(req,res)
}

export function add_project_to_portfolio(req,res) {
    databaseController.add_project_to_portfolio(req,res)
}

export function get_portfolio_skills(req,res) {
    databaseController.get_portfolio_skills(req,res)
}

export function get_portfolio_projects(req,res) {
    databaseController.get_portfolio_projects(req,res)
}