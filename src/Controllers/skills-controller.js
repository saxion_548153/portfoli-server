import * as databaseController from '../Database/database_helper.js'

export function create_new_skill(req, res) {
    databaseController.create_new_skill(req,res)
}

export function get_all_skills(req, res) {
    databaseController.get_all_skills(req,res)
}

export function get_skill(req, res) {
    databaseController.get_skill(req,res)
}

export function delete_skill(req, res) {
    databaseController.delete_skill(req,res)
}

export function update_skill(req,res) {
    databaseController.update_skill(req,res)
}