import * as databaseController from '../Database/database_helper.js'

export function create_new_project(req, res) {
    databaseController.create_new_project(req,res)
}

export function get_all_projects(req, res) {
    databaseController.get_all_projects(req,res)
}

export function get_project(req, res) {
    databaseController.get_project(req,res)
}

export function delete_project(req, res) {
    databaseController.delete_project(req,res)
}

export function update_project(req,res) {
    databaseController.update_project(req,res)
}